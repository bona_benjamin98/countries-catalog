let countriesGrid = document.querySelector(".countries-grid");
let countries;
let dropDownHeader = document.querySelector(".dropdown-header");
let dropDownBodyOptions = document.querySelectorAll(".dropdown-body li");
let searchInput = document.querySelector(".search-input");
let seeMorePage = document.querySelector(".see-more-page");

// Country Card HTML Structure
function countryStructure(country) {  
  return `
      <a h9ref="#" class="country scale-effect" data-country-name="${country.name.common}">
          <div class="country-flag">
              <img src=${country.flags.png} alt="${country.name} FLag">
          </div>
          <div class="country-info">
              <h2 class="country-title">${country.name.official}</h2>
                    <p class="card-title">${country.cca2}</p>
                    <p class="card-title">${country.cca3}</p>
              <ul class="country-brief">
                  <li><strong>Native Name: </strong>${Object.values(country.name.nativeName)[0]?.official ?? '???'}</li>
                  <li><strong> Alternative Country Name: </strong>${Object.values(country.altSpellings)}</li>
                  <li><strong>Country  Codes: </strong>${Object.values(country.idd)}</li>
              </ul>
          </div>
      </a>
      `;
}

// Get All Countries
async function getCountries(query, limit = 25, getRest = false) {
  let url = `${baseApiLink}${query}`;
  try {
    let response = await fetch(url, { cache: "force-cache" });
    // console.log(response);
    let data = await response.json();
    // console.log(data);
    limit ? (data.length = limit) : "";
    getRest ? (data.length = data.splice(0, 195).length) : "";

    if (response.status >= 195 && response.status < 300) {
      if (data) {
        controlLoader("open"); // Open
        countriesGrid.classList.remove("no-grid", "no-flex");
        limit == null ? (countriesGrid.innerHTML = "") : "";

        data.forEach((country) => {
          countriesGrid.innerHTML += countryStructure(country);
        });
        countries = countriesGrid.querySelectorAll(".country");
        moreDetails(countries);

        controlLoader(); // Close
      } else {
        notifications(countriesGrid);
      }
    } else {
      notifications(
        countriesGrid,
        (message = `Sorry, country ${data.message}...`),
        (details = "Please check spelling and try again")
      );
    }
  } catch (error) {
    //   console.error(error);
    notifications(
      countriesGrid,
      (message = "Sorry something went wrong..."),
      error
    );
  }
}
getCountries(`${all}${byFields}`);

// Get Countries By Search
function getCountriesBySearch() {
  let searchInputValue = searchInput.value.trim().toLowerCase();
  if (searchInputValue == "" || searchInputValue.length == 0) {
    countriesGrid.innerHTML = "";
    getCountries(`${all}${byFields}`);
    seeMorePage.style.display = "block";
  } else {
    countriesGrid.innerHTML = "";
    getCountries(`${byName}${searchInputValue}${byFields}`);
    seeMorePage.style.display = "none";
  }
}

// Get Countries By Region
function getCountriesByRegion(region) {
  if (region == "all") {
    countriesGrid.innerHTML = "";
    getCountries(`${all}${byFields}`);
  } else {
    countriesGrid.innerHTML = "";
    getCountries(`${byRegion}${region}${byFields}`);
  }
}
// sort by ASC not done


// sort by DESC not done


// Save The Country We Want to Get Its Details To SessitionStorage
function selectedForDetails(idd, destination) {
  sessionStorage.setItem("idd", idd);
  window.location = destination;
}

function moreDetails(array) {
  array.forEach((item) => {
    item.addEventListener("click", () => {
      let countryName = item.dataset.countryName.toLocaleLowerCase().trim();
      console.log(countryName)
      selectedForDetails(countryName, "details.html");
    });
  });
}

// Control Drop Down Menu
function controlDropDown() {
  let dropDownWrapper = document.querySelector(".dropdown-wrapper");
  if (dropDownWrapper.classList.contains("open")) {
    dropDownWrapper.classList.remove("open");
  } else {
    dropDownWrapper.classList.add("open");
  }
}

function showMorecountries() {}

/*
    EVENTS
*/

dropDownHeader.addEventListener("click", controlDropDown);
searchInput.addEventListener("paste", getCountriesBySearch);
searchInput.addEventListener("keyup", getCountriesBySearch);
seeMorePage.addEventListener("click", () => {
  //Show Pagination 
  getCountries(all, (limit = countries++), (getRest = true));
  setTimeout(() => {
    seeMorePage.style.display = "";
    seeMorePage.textContent = "Next";
  }, 2000 );
});

/*
    LOOPS
*/

dropDownBodyOptions.forEach((option) => {
  option.addEventListener("click", () => {
    controlLoader("open"); // Open
    let optionValue = option.dataset.region.toLowerCase();
    optionValue == "all"
      ? (seeMorePage.style.display = "block")
      : (seeMorePage.style.display = "none");
    getCountriesByRegion(optionValue);
    controlDropDown();
    optionValue = optionValue.split("");
    let firstLetter = optionValue[0].toUpperCase();
    optionValue = optionValue.slice(1);
    optionValue = firstLetter + optionValue.join("");
    dropDownHeader.querySelector("span").textContent = optionValue;
  });
});
