let detailsGrid = document.querySelector(".country-details");
let borderCountries;
byFields = `?fields=flag,flags,name,cca2,cca3,idd,altSpellings,population,region,subregion,capital,tld,currencies,languages,borders`;

// Utils function
const objectMap = (obj, fn) =>
  Object.fromEntries(
    Object.entries(obj).map(
      ([k, v], i) => [k, fn(v, k, i)]
    )
  )


/*
    FUNCTIONS
*/

// Country Details 
function countryDetailsStructure(country) {
  return `
    <div class="country-flag">
      <img src=${country.flags.png} alt="${country.flag} Flag" style="margin: 0;" />
    </div>
    <div class="country-info">
      <div class="col col-1">
        <h1 class="country-title">${country.name.official}</h1>
        <p class="card-title">${country.cca2}</p>
        <p class="card-title">${country.cca3}</p>
      </div>
      <div class="col col-2">
        <div class="col col-1">
          <ul>
            <li><strong>native name: </strong> ${Object.values(country.name.nativeName)[0]?.official ?? '???'}</li>
            <li><strong> Alternative Country Name: </strong>${Object.values(country.altSpellings)}</li>
            <li><strong>Country  Codes: </strong>${Object.values(country.idd)}</li>
            <li><strong>population: </strong> ${country.population}</li>
            <li><strong>region: </strong> ${country.region}</li>
            <li><strong>sub region: </strong> ${country.subregion}</li>
            <li><strong>capital: </strong> ${country.capital}</li>
          </ul>
        </div>
        <div class="col col-2">
          <ul>
            <li><strong>top level domain: </strong> ${country.tld}</li>
            <li><strong>currencies: </strong> ${Object.values(country.currencies)[0]?.name ?? '???'}</li>
            <li><strong>languages: </strong> ${Object.values(country.languages).join(", ")}</li>
          </ul>
        </div>
      </div>
      <div class="col col-3">
      ${
        country.borders == undefined
          ? "<strong class='warning'>no borders for this country...!</strong>"
          : `<strong> border countries:</strong> ${`
        <ul>
            ${country.borders
              .map(
                (border) => `
                <li data-border=${border} onclick="moreDetails(this)">
                <button
                  type="button"
                  class="button btn"
                  data-country-name="${country.name}"
                >
                ${border}
                </button>
              </li>`
              )
              .join("")}
          </ul>
      `}`
      }      
      </div>
    </div>
        `;
}

// Get Country Details
async function getCountryDetails() {
  let sessionValue = sessionStorage.getItem("idd");
  // try {
    let response = await fetch(
      `${baseApiLink}${byName}${sessionValue}${byFields}&fullText=true`
    );
    // console.log(response);
    let data = await response.json();
    console.log(data);

    if (response.status == 404) {
      notifications(
        detailsGrid,
        (message = `Sorry, country ${data.message}...`),
        (details = "Please check spelling and try again")
      );
    } else {
      if (data) {
        controlLoader("open"); // Open
        detailsGrid.classList.remove("no-grid", "no-flex");
        detailsGrid.innerHTML = "";
        data.forEach((country) => {
          detailsGrid.innerHTML += countryDetailsStructure(country);
        });
        borderCountries = document.querySelectorAll(".col-3 li");
        controlLoader(); // Close
      } else {
        notifications(detailsGrid);
      }
    }
 
}
getCountryDetails();

// Get Border Countries Details
async function getBorderDetails(value) {
  try {
    let response = await fetch(`${baseApiLink}${byAlpha}${value}${byFields}`);
    // console.log(response);
    let data = await response.json();
    // console.log(data);

    if (response.status == 404) {
      notifications(
        detailsGrid,
        (message = `Sorry, country ${data.message}...`),
        (details = "Please check spelling and try again")
      );
    } else {
      if (data) {
        controlLoader("open"); // Open
        detailsGrid.classList.remove("no-grid", "no-flex");
        detailsGrid.innerHTML = "";
        detailsGrid.innerHTML += countryDetailsStructure(data);
        controlLoader(); // Close
      } else {
        notifications(detailsGrid);
      }
    }
  } catch (error) {
    //   console.error(error);
    notifications(
      detailsGrid,
      (message = "Sorry something went wrong..."),
      error
    );
  }
}

function moreDetails(el) {
  controlLoader("open"); // Open
  let countryName = el.dataset.border.toLocaleLowerCase().trim();
  console.log(countryName)
  getBorderDetails(countryName);
}
